# technologies_importer
Smartly import technologies from OTM's sophia database through an exported CSV file.

##TO DO:
------

~~Only update field if new data exists where no data did before. Right now, it's just set to update to whatever the new data is, reguardless if there was old data or not.~~

Need to check the marketing fields for the above. I wasn't sure how to handle those fields since the code is a bit different.

| Old Data | New Data | Is Updating | Should Update |
| ------------- | ------------- | ------------- | ------------- |
| ABCD  | ABCD  | No  | No  | 
| ABCD  | DCBA  | Yes  | No  | 
| -blank-  | ABCD  | Yes  | Yes  | 

The content type needs a nice looking template, so that the actual listings look nice.

The content type needs a page view, so there's a page that paginates through the listings. It should be filterable by taxonomy as well as the status field. The view needs to look good. 

~~The content type needs a field for uploading PDFs. The PDFs should be downloadable if status is set to Market and Licensed.~~


~~Create example CSV file. 3 columns by 10 rows, or so? Something we can import over and over.~~

###.Install
* ~~define content type fields and instances. (Look at s51 slider module on how to create content type.)~~
* ~~start with a very simple content type. a title and a few text fields.~~
* ~~uninstall content type~~

###.Module
* If CSV is malformed, display a message indicating that's the case instead of the generic drupal error.
* ~~Colorcode the results.~~
* ~~Set up admin page where CSV files can be uploaded from and freshly imported data can be reviewed.~~ 
* ~~Set up permission so that only roles with the permission enabled can import new CSV files~~



