$ = jQuery;

function colorRows() {

	$('.ti_results_table .color:contains("green")').parent().parent().addClass('green');
	$('.ti_results_table .color:contains("yellow")').parent().parent().addClass('yellow');
	$('.ti_results_table .color:contains("red")').parent().parent().addClass('red');
}

function hidePDF() {	
	if ($('.field--name-ti-status .field__item:contains("Market and Licensed")').length > 0) {
		//$('.field--name-ti-pdf').remove();
	}
	else {
		$('.block--fieldblock-node-technology-default-ti-pdf').remove();

	}
}


// When the page first loads.
$(document).ready(function() {
	colorRows();
	hidePDF();
});

// When everything on the page has loaded.
$(window).bind("load", function() {
	hidePDF();
});

// When the page is scrolled
$(window).scroll(function() {

});

// When the page is resized
$(window).resize(function() {

});